#!/bin/bash

set -euo pipefail

echo "installing dev baseline"
apk add --no-cache libffi-dev alpine-sdk

echo "Installing pyNaCl from alpine"
apk add --no-cache py3-pynacl yaml-dev

if [[ "${UPDATE_PIP_REQUIREMENTS:-false}" == "true" ]]; then
  echo "installing pipenv"
  pip install --no-cache-dir pipenv

  echo "locking requirements"
  pipenv lock -r > requirements.txt
fi

echo "requirements identified"
cat requirements.txt

echo "installing"
pip install --no-cache-dir -r requirements.txt

echo "removing dependencies"
apk del libffi-dev alpine-sdk

echo "uninstalling pipenv"
pip uninstall -y pipenv

rm requirements.txt
rm Pipfile.lock
rm Pipfile
rm build.sh

echo "done"
